﻿namespace GeneradorTurnos
{
    partial class frmGenerador
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTurnos = new System.Windows.Forms.Panel();
            this.lblNumero15 = new System.Windows.Forms.Label();
            this.lblNumero14 = new System.Windows.Forms.Label();
            this.lblNumero13 = new System.Windows.Forms.Label();
            this.lblNumero11 = new System.Windows.Forms.Label();
            this.lblNumero16 = new System.Windows.Forms.Label();
            this.lblNumero8 = new System.Windows.Forms.Label();
            this.lblNumero10 = new System.Windows.Forms.Label();
            this.lblNumero12 = new System.Windows.Forms.Label();
            this.lblNumero2 = new System.Windows.Forms.Label();
            this.lblNumero6 = new System.Windows.Forms.Label();
            this.lblNumero17 = new System.Windows.Forms.Label();
            this.lblNumero3 = new System.Windows.Forms.Label();
            this.lblNumero4 = new System.Windows.Forms.Label();
            this.lblNumero7 = new System.Windows.Forms.Label();
            this.lblNumero5 = new System.Windows.Forms.Label();
            this.lblNumero1 = new System.Windows.Forms.Label();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.lblNumeroElegido = new System.Windows.Forms.Label();
            this.panelTurnos.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTurnos
            // 
            this.panelTurnos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTurnos.Controls.Add(this.lblNumero15);
            this.panelTurnos.Controls.Add(this.lblNumero14);
            this.panelTurnos.Controls.Add(this.lblNumero13);
            this.panelTurnos.Controls.Add(this.lblNumero11);
            this.panelTurnos.Controls.Add(this.lblNumero16);
            this.panelTurnos.Controls.Add(this.lblNumero8);
            this.panelTurnos.Controls.Add(this.lblNumero10);
            this.panelTurnos.Controls.Add(this.lblNumero12);
            this.panelTurnos.Controls.Add(this.lblNumero2);
            this.panelTurnos.Controls.Add(this.lblNumero6);
            this.panelTurnos.Controls.Add(this.lblNumero17);
            this.panelTurnos.Controls.Add(this.lblNumero3);
            this.panelTurnos.Controls.Add(this.lblNumero4);
            this.panelTurnos.Controls.Add(this.lblNumero7);
            this.panelTurnos.Controls.Add(this.lblNumero5);
            this.panelTurnos.Controls.Add(this.lblNumero1);
            this.panelTurnos.Location = new System.Drawing.Point(12, 12);
            this.panelTurnos.Name = "panelTurnos";
            this.panelTurnos.Size = new System.Drawing.Size(400, 400);
            this.panelTurnos.TabIndex = 0;
            // 
            // lblNumero15
            // 
            this.lblNumero15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero15.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero15.Location = new System.Drawing.Point(117, 310);
            this.lblNumero15.Name = "lblNumero15";
            this.lblNumero15.Size = new System.Drawing.Size(70, 70);
            this.lblNumero15.TabIndex = 19;
            this.lblNumero15.Text = "15";
            this.lblNumero15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero14
            // 
            this.lblNumero14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero14.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero14.Location = new System.Drawing.Point(19, 310);
            this.lblNumero14.Name = "lblNumero14";
            this.lblNumero14.Size = new System.Drawing.Size(70, 70);
            this.lblNumero14.TabIndex = 17;
            this.lblNumero14.Text = "14";
            this.lblNumero14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero13
            // 
            this.lblNumero13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero13.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero13.Location = new System.Drawing.Point(308, 213);
            this.lblNumero13.Name = "lblNumero13";
            this.lblNumero13.Size = new System.Drawing.Size(70, 70);
            this.lblNumero13.TabIndex = 16;
            this.lblNumero13.Text = "13";
            this.lblNumero13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero11
            // 
            this.lblNumero11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero11.Location = new System.Drawing.Point(117, 213);
            this.lblNumero11.Name = "lblNumero11";
            this.lblNumero11.Size = new System.Drawing.Size(70, 70);
            this.lblNumero11.TabIndex = 13;
            this.lblNumero11.Text = "11";
            this.lblNumero11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero16
            // 
            this.lblNumero16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero16.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero16.Location = new System.Drawing.Point(215, 310);
            this.lblNumero16.Name = "lblNumero16";
            this.lblNumero16.Size = new System.Drawing.Size(70, 70);
            this.lblNumero16.TabIndex = 12;
            this.lblNumero16.Text = "16";
            this.lblNumero16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero8
            // 
            this.lblNumero8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero8.Location = new System.Drawing.Point(308, 115);
            this.lblNumero8.Name = "lblNumero8";
            this.lblNumero8.Size = new System.Drawing.Size(70, 70);
            this.lblNumero8.TabIndex = 11;
            this.lblNumero8.Text = "8";
            this.lblNumero8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero10
            // 
            this.lblNumero10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero10.Location = new System.Drawing.Point(19, 213);
            this.lblNumero10.Name = "lblNumero10";
            this.lblNumero10.Size = new System.Drawing.Size(70, 70);
            this.lblNumero10.TabIndex = 9;
            this.lblNumero10.Text = "10";
            this.lblNumero10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero12
            // 
            this.lblNumero12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero12.Location = new System.Drawing.Point(215, 213);
            this.lblNumero12.Name = "lblNumero12";
            this.lblNumero12.Size = new System.Drawing.Size(70, 70);
            this.lblNumero12.TabIndex = 8;
            this.lblNumero12.Text = "12";
            this.lblNumero12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero2
            // 
            this.lblNumero2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero2.Location = new System.Drawing.Point(117, 16);
            this.lblNumero2.Name = "lblNumero2";
            this.lblNumero2.Size = new System.Drawing.Size(70, 70);
            this.lblNumero2.TabIndex = 7;
            this.lblNumero2.Text = "2";
            this.lblNumero2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero6
            // 
            this.lblNumero6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero6.Location = new System.Drawing.Point(117, 115);
            this.lblNumero6.Name = "lblNumero6";
            this.lblNumero6.Size = new System.Drawing.Size(70, 70);
            this.lblNumero6.TabIndex = 6;
            this.lblNumero6.Text = "6";
            this.lblNumero6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero17
            // 
            this.lblNumero17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero17.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero17.Location = new System.Drawing.Point(308, 310);
            this.lblNumero17.Name = "lblNumero17";
            this.lblNumero17.Size = new System.Drawing.Size(70, 70);
            this.lblNumero17.TabIndex = 5;
            this.lblNumero17.Text = "17";
            this.lblNumero17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero3
            // 
            this.lblNumero3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero3.Location = new System.Drawing.Point(215, 16);
            this.lblNumero3.Name = "lblNumero3";
            this.lblNumero3.Size = new System.Drawing.Size(70, 70);
            this.lblNumero3.TabIndex = 4;
            this.lblNumero3.Text = "3";
            this.lblNumero3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero4
            // 
            this.lblNumero4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero4.Location = new System.Drawing.Point(308, 16);
            this.lblNumero4.Name = "lblNumero4";
            this.lblNumero4.Size = new System.Drawing.Size(70, 70);
            this.lblNumero4.TabIndex = 3;
            this.lblNumero4.Text = "4";
            this.lblNumero4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero7
            // 
            this.lblNumero7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero7.Location = new System.Drawing.Point(215, 115);
            this.lblNumero7.Name = "lblNumero7";
            this.lblNumero7.Size = new System.Drawing.Size(70, 70);
            this.lblNumero7.TabIndex = 2;
            this.lblNumero7.Text = "7";
            this.lblNumero7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero5
            // 
            this.lblNumero5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero5.Location = new System.Drawing.Point(19, 115);
            this.lblNumero5.Name = "lblNumero5";
            this.lblNumero5.Size = new System.Drawing.Size(70, 70);
            this.lblNumero5.TabIndex = 1;
            this.lblNumero5.Text = "5";
            this.lblNumero5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumero1
            // 
            this.lblNumero1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero1.Location = new System.Drawing.Point(19, 16);
            this.lblNumero1.Name = "lblNumero1";
            this.lblNumero1.Size = new System.Drawing.Size(70, 70);
            this.lblNumero1.TabIndex = 0;
            this.lblNumero1.Text = "1";
            this.lblNumero1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(106, 451);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(104, 36);
            this.btnIniciar.TabIndex = 1;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // lblNumeroElegido
            // 
            this.lblNumeroElegido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroElegido.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroElegido.Location = new System.Drawing.Point(238, 426);
            this.lblNumeroElegido.Name = "lblNumeroElegido";
            this.lblNumeroElegido.Size = new System.Drawing.Size(70, 70);
            this.lblNumeroElegido.TabIndex = 6;
            this.lblNumeroElegido.Text = "?";
            this.lblNumeroElegido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmGenerador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 506);
            this.Controls.Add(this.lblNumeroElegido);
            this.Controls.Add(this.btnIniciar);
            this.Controls.Add(this.panelTurnos);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGenerador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generador de turnos";
            this.panelTurnos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTurnos;
        private System.Windows.Forms.Label lblNumero1;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Label lblNumero15;
        private System.Windows.Forms.Label lblNumero14;
        private System.Windows.Forms.Label lblNumero13;
        private System.Windows.Forms.Label lblNumero11;
        private System.Windows.Forms.Label lblNumero16;
        private System.Windows.Forms.Label lblNumero8;
        private System.Windows.Forms.Label lblNumero10;
        private System.Windows.Forms.Label lblNumero12;
        private System.Windows.Forms.Label lblNumero2;
        private System.Windows.Forms.Label lblNumero6;
        private System.Windows.Forms.Label lblNumero17;
        private System.Windows.Forms.Label lblNumero3;
        private System.Windows.Forms.Label lblNumero4;
        private System.Windows.Forms.Label lblNumero7;
        private System.Windows.Forms.Label lblNumero5;
        private System.Windows.Forms.Label lblNumeroElegido;
    }
}

