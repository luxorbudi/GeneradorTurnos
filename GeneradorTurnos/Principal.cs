﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace GeneradorTurnos {
    public partial class frmGenerador : Form {        
        List<int> listaGrupos = new List<int>();
        Point posElegido;
        Point posPantalla;
        
        public frmGenerador() {
            InitializeComponent();
            listaGrupos.AddRange(Enumerable.Range(1, 17).Where(x => x != 9));
        }

        private void btnIniciar_Click(object sender, EventArgs e) {
            btnIniciar.Enabled = false;
            if (listaGrupos.Count() > 0) {
                ((Label)this.Controls["panelTurnos"]
                            .Controls["lblNumero" + generarNumero().ToString()])
                            .ForeColor = Color.Blue;
                lblNumeroElegido.ForeColor = Color.Blue;
                animar(true);
            }
            else {                
                MessageBox.Show("Terminó la clase", "Fin de presentaciones", MessageBoxButtons.OK);
            }
            Thread.Sleep(1000);
            animar(false); 
            lblNumeroElegido.ForeColor = Color.Black;
            btnIniciar.Enabled = true;
        }

        private void animar(bool mostrar) {
            if (mostrar)
            {
                posElegido = new Point(lblNumeroElegido.Location.X + lblNumeroElegido.Width / 2,
                                    lblNumeroElegido.Location.Y + lblNumeroElegido.Height / 2);

                //Point posElegido = new Point(lblNumeroElegido.Location.X, lblNumeroElegido.Location.Y);

                posPantalla = new Point(this.panelTurnos.Location.X + this.panelTurnos.Width / 2,
                                                this.panelTurnos.Location.Y + this.panelTurnos.Height / 2);

                //Point posPantalla = new Point(this.panelTurnos.Location.X, this.panelTurnos.Location.Y);
            }
            
            
            double distancia = Math.Round(Math.Sqrt(Math.Pow(posPantalla.X - posElegido.X, 2) + 
                                                    Math.Pow(posPantalla.Y - posElegido.Y, 2)));
            double angulo = Math.Atan2(posElegido.Y - posPantalla.Y,
                                       posElegido.X - posPantalla.X);    
            int cantidadDivisiones = 20;            
            double distanciaProporcional = mostrar?distancia / cantidadDivisiones:-distancia/cantidadDivisiones;
            float letrainicial,letrafinal;
            if (mostrar) {
                letrainicial = 26.25f;
                letrafinal = 100f;
            }
            else
            {
                letrainicial = 100f;
                letrafinal = 26.25f;
            }
            
            float crece = (letrafinal-letrainicial)/cantidadDivisiones;
            int retX = (int)(distanciaProporcional * Math.Cos(angulo));
            int retY = (int)(distanciaProporcional * Math.Sin(angulo));

            for (int i = 0; i < cantidadDivisiones; i++) {
                int n = mostrar ? 1 : -1;
                Point nuevaDistancia = new Point(lblNumeroElegido.Location.X - (int)retX - (i/2*n), lblNumeroElegido.Location.Y - (int)retY - (i/2*n));
                lblNumeroElegido.SetBounds(nuevaDistancia.X,
                                           nuevaDistancia.Y,
                                           lblNumeroElegido.Bounds.Width + (i*n),
                                           lblNumeroElegido.Bounds.Height + (i*n));
                lblNumeroElegido.Font = new Font(lblNumeroElegido.Name,letrainicial= letrainicial+crece,lblNumeroElegido.Font.Style,lblNumeroElegido.Font.Unit);
                Application.DoEvents();
                Thread.Sleep(10);
            }
        }

        private int generarNumero() {
            int numeroElegido = 0; 

            Enumerable.Range(1, 60).ToList().ForEach(x => {
                numeroElegido = listaGrupos.ElementAt(new Random().Next(0, listaGrupos.Count));
                lblNumeroElegido.Text = numeroElegido.ToString();
                Application.DoEvents();
                Thread.Sleep(10);
            });
           
            listaGrupos.Remove(numeroElegido);
            return numeroElegido;
        }
    }
}
